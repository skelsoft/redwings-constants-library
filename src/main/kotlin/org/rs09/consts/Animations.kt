package org.rs09.consts

object Animations {

    const val HUMAN_OPEN_CHEST_536 = 536
    const val PARTY_PETE_NOD_TWIRL_DANCE_784 = 784
    const val HUMAN_BURYING_BONES_827 = 827
    const val HUMAN_EATTING_829 = 829
    const val HUMAN_BANK_DEPOSIT_BOX_834 = 834
    const val HUMAN_CLIMB_OVER_MEDIUM_839 = 839
    const val HUMAN_SHAKE_HEAD_YES_855 = 855
    const val HUMAN_SHAKE_HEAD_NO_856 = 856
    const val HUMAN_THINK_857 = 857
    const val HUMAN_BOW_858 = 858
    const val HUMAN_ANGRY_859 = 859
    const val HUMAN_CRY_860 = 860
    const val HUMAN_LAUGH_861 = 861
    const val HUMAN_CHEER_862 = 862
    const val HUMAN_WAVE_863 = 863
    const val HUMAN_BECKON_864 = 864
    const val HUMAN_CLAP_865 = 865
    const val HUMAN_DANCE_866 = 866
    const val HUMAN_PICKPOCKETING_881 = 881
    const val HUMAN_PLAY_WITH_BROWN_HORSE_918 = 918
    const val HUMAN_PLAY_WITH_WHITE_HORSE_919 = 919
    const val HUMAN_PLAY_WITH_BLACK_HORSE_920 = 920
    const val HUMAN_PLAY_WITH_GRAY_HORSE_921 = 921
    const val HUMAN_GLASS_WALL_1128 = 1128
    const val HUMAN_LEAN_1129 = 1129
    const val HUMAN_CLIMB_ROPE_1130 = 1130
    const val HUMAN_GLASS_BOX_1131 = 1131
    const val MOUNTAIN_TROLL_THROW_ROCK_1142 = 1142
    const val HUMAN_DRINKING_1327 = 1327
    const val HUMAN_BLOW_KISS_1374 = 1374
    const val HUMAN_SPINNING_PLATE_START_1902 = 1902
    const val HUMAN_SPINNING_PLATE_SUCCESS_1904 = 1904
    const val HUMAN_SPINNING_PLATE_FAIL_1906 = 1906
    const val HUMAN_PANIC_2105 = 2105
    const val HUMAN_JIG_2106 = 2106
    const val HUMAN_TWIRL_2107 = 2107
    const val HUMAN_HEAD_BANG_2108 = 2108
    const val HUMAN_JUMP_FOR_JOY_2109 = 2109
    const val HUMAN_BLOW_RASPBERRY_2110 = 2110
    const val HUMAN_YAWN_2111 = 2111
    const val HUMAN_SALUTE_2112 = 2112
    const val HUMAN_SHRUG_2113 = 2113
    const val HUMAN_GOBLIN_BOW_2127 = 2127
    const val HUMAN_GOBLIN_SALUTE_2128 = 2128
    const val HUMAN_AIR_GUITAR_2414 = 2414
    const val HUMAN_PLAY_WITH_FISHBOWL_2780 = 2780
    const val HUMAN_FEED_FISHBOWL_2781 = 2781
    const val HUMAN_TALK_WITH_FISHBOWL_2782 = 2782
    const val HUMAN_PLAY_WITH_FISHBOWL2_2783 = 2783
    const val HUMAN_FEED_FISHBOWL2_2784 = 2784
    const val HUMAN_TALK_WITH_FISHBOWL2_2785 = 2785
    const val HUMAN_PLAY_WITH_FISHBOWL3_2786 = 2786
    const val HUMAN_FEED_FISHBOWL3_2787 = 2787
    const val HUMAN_TALK_WITH_FISHBOWL3_2788 = 2788
    const val HUMAN_SCARED_2836 = 2836
    const val DARKLIGHT_SPECIAL_ATTACK_2890 = 2890
    const val HUMAN_ZOMBIE_DANCE_3543 = 3543
    const val HUMAN_ZOMBIE_WALK_3544 = 3544
    const val HUMAN_ENHANCED_FLAP_3859 = 3859
    const val HUMAN_SLAP_HEAD_4275 = 4275
    const val HUMAN_IDEA_4276 = 4276
    const val HUMAN_STOMP_4278 = 4278
    const val HUMAN_FLAP_4280 = 4280
    const val TRAIBORN_SUMMON_CABINET_4602 = 4602
    const val WALLY_SILVERLIGHT_STAB_4603 = 4603
    const val HUMAN_RECEIVE_SILVERLIGHT_4604 = 4604
    const val HUMAN_CURTSY_5312 = 5312
    const val HUMAN_ENHANCED_YAWN_5313 = 5313
    const val HUMAN_ENHANCED_ANGRY_5315 = 5315
    const val HUMAN_ENHANCED_DANCE_5316 = 5316
    const val HUMAN_ENHANCED_BECKON_5845 = 5845
    const val HUMAN_BUNNY_HOP_6111 = 6111
    const val HUMAN_STAT_SPY_6293 = 6293
    const val HUMAN_PARTY_ROOM_LEVER_6933 = 6933
    const val HUMAN_SPIRIT_TREE_TELE_TO_7082 = 7082
    const val HUMAN_SPIRIT_TREE_TELE_FROM_7084 = 7084
    const val HUMAN_ZOMBIE_HAND_7272 = 7272
    const val HUMAN_SNOWMAN_DANCE_7531 = 7531
    const val HUMAN_SAFETY_FIRST_8770 = 8770
    const val HUMAN_DRAGON_KITE_FLYING_8990 = 8990
    const val HUMAN_EXPLORE_9990 = 9990
    const val HUMAN_TRICK_10530 = 10530
    const val HUMAN_TURKEY_TRANSFORMATION_10994 = 10994
    const val HUMAN_TURKEY_RETURN_10995 = 10995
    const val HUMAN_TURKEY_DANCE_10996 = 10996
    const val HUMAN_FALADOR_SHIELD_PRAYER_RESTORE_11012 = 11012
    const val HUMAN_FREEZE_AND_MELT_11044 = 11044

}